---
title: Help give Remote.com a new look
date: "2019-03-07"
description: We’re rebuilding Remote.com from scratch. We want you to also feel like it’s something new and different. To that end, we’ve decided to do a complete rebranding of Remote.com. We will be doing this in the open, and we need your help.
image: "u3.svg"
socialImage: "u3.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
---



[We’re rebuilding Remote.com from scratch][post]. We want you to also feel like it’s something new and different. To that end, we’ve decided to do a complete rebranding of Remote.com. We will be doing this in the open, and we need your help.

[post]: https://blog.remote.com/we-re-rebuilding-remote-with-a-new-team/

## Process of rebranding

We’re looking to build a new image of Remote.com. That means color, tone, and even a new logo. These choices will also influence how the rest of Remote.com will look, but they are merely a guideline for the interface, not a template. 

There are several components leading up to us releasing the new branding. Everything we will be doing related to these will be open to view and contribute (!). Below a high level overview of the components.

1. Market research
1. Define the intention behind the brand, as a [design persona](https://alistapart.com/article/crafting-a-design-persona)
1. From that, create a few design directions for the brand identity, in the form of naming, logo, tagline, etc.
1. Choose one direction forward and create brand guidelines (includes colors, typography, illustration/photographic styles)

We’re working together with [Pedro Moreira da Silva](https://twitter.com/PedroMScom) (an amazing designer and friend), and everyone that wants to participate. All the work, and communication takes place in [our branding project][gitlab]. It’s public, so you can easily view and contribute there. Even the [meeting agendas](https://docs.google.com/document/d/1jy4NWOj6Zi0GoweihE82tandeQ379TOlh0nbp-K2QtQ/edit?usp=sharing) are public, and we intend to share the recordings of future meetings on YouTube.

Besides the work in our project, we will be sending out a [survey][survey] to whomever is interested in giving their opinion of several different design directions.

Finally, our brand guidelines and design persona will be made available in our [handbook](https://handbook.remote.com/) or on our website.

## How you can help

As said, we want your help! Whether you run a company, are looking for a job, or neither of those, your feedback is what we want. 

1. [Sign up to be part of our survey][survey]: we will send you an email once we have initial design directions ready and will ask for your opinion. We will share the aggregated results of this later on.
1. Look at, and consider contributing directly to our [branding project][gitlab]. Leave your opinion, own designs, whatever you want. 

We will also release updates on this blog when we have them. Ultimately, we hope to create something that you’ll enjoy, so your input is very important to the success of the rebranding.

[survey]: https://goo.gl/forms/W8hcmvxwWVDkBGpg1 
[gitlab]: https://gitlab.com/remote-com/branding/issues 
