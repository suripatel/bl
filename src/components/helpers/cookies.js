export function setDarkModeCookieValue(value) {
  document.cookie = `dark=${value}; max-age=31536000; path=/`
  document.cookie = `dark=${value}; max-age=31536000; domain=remote.com; path=/`
}

export function getDarkModeCookieValue() {
  let value = "light"

  if (
    document.cookie.split(";").filter(function(item) {
      return item.indexOf("dark=true") >= 0
    }).length
  ) {
    value = "dark"
  }

  return value
}
