import React from "react"
import styled, { ThemeProvider, createGlobalStyle } from "styled-components"

import { rhythm } from "../utils/typography"
import { Backgrounds } from "./darkMode"
import {
  getDarkModeCookieValue,
  setDarkModeCookieValue,
} from "../components/helpers/cookies"

import "../assets/css/fonts.css"
import Header from "./header"
import Footer from "./footer"

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${Backgrounds};
    transition: color 0.2s ease-out, background 0.2s ease-out;
  }
`

const MainContent = styled.main`
  padding-top: 20px;
`

const LayoutContainer = styled.section`
  max-width: ${rhythm(26)};
  margin: 0 auto;
  padding: 65px ${rhythm(3 / 4)};
  font-family: "Inter";
`

class Layout extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      mode: "light",
    }

    this.toggleMode = this.toggleMode.bind(this)
  }

  componentDidMount() {
    this.setState({ mode: getDarkModeCookieValue() })
  }

  toggleMode() {
    this.setState(
      prevState => ({
        mode: prevState.mode === "light" ? "dark" : "light",
      }),
      () => setDarkModeCookieValue(this.state.mode === "dark" ? true : false)
    )
  }

  render() {
    const { location, title, children } = this.props
    const { mode } = this.state

    return (
      <ThemeProvider theme={{ mode: mode }}>
        <LayoutContainer>
          <GlobalStyle />
          <Header
            location={location}
            title={title}
            toggleMode={this.toggleMode}
            mode={mode}
          />
          <MainContent>{children}</MainContent>
          <Footer toggleMode={this.toggleMode} />
        </LayoutContainer>
      </ThemeProvider>
    )
  }
}

export default Layout
